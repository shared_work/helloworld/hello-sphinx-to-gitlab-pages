#!/bin/bash

# ------------------------------------------------------------------------------
# Rename all image files in the source directory to be compatible with Sphinx.
#
# Examples:
#   ./clean-images
#   ./clean-images --debug
# ------------------------------------------------------------------------------

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR="$(realpath "$SCRIPT_DIR/..")"

# shellcheck disable=SC1090
source "$SCRIPT_DIR/variables.sh"
# shellcheck disable=SC1090
source "$SCRIPT_DIR/commons.sh"

function main() {
    local debug=false

    if [ "$#" -gt 0 ]; then
        case "$1" in
            --debug)   debug=true ;;
            -h|--help) help ; exit 0 ;;
            *)         help ; exit 1 ;;
        esac
    fi

    print_line
    print_h1 "Renaming images"
    print_line

    local source_dir="$PROJECT_DIR/source"
    local extensions=(bmp gif ico jpg jpeg png tiff)

    print_h1_highlight "Directory .... $source_dir"
    print_h1_highlight "Extensions ... $(join_by , "${extensions[@]}")"
    print_line

    local extensions_as_regex
    extensions_as_regex=$(join_by "|" "${extensions[@]}")

    readarray -d '' file_paths < <(\
        find "$PROJECT_DIR/source" \
        -regextype posix-extended -regex "^.+\\.($extensions_as_regex)$" \
        -print0 \
    )

    print_step_information "Found ${#file_paths[@]} image file(s) to check"
    print_line

    local renamed_files=0

    for file_path in "${file_paths[@]}"; do
      [ "$debug" = true ] && print_line " - Evaluating $file_path"

      local file_name
      local file_directory
      local new_file_name
      local new_file_path

      file_name="$(basename "$file_path")"
      file_directory="$(dirname "$file_path")"

      new_file_name=$( \
          echo "$file_name"       \
          | sed -r                \
                -e 's/ /_/g'      \
                -e 's/\[//g'      \
                -e 's/\]//g'      \
                -e 's/[àâä]/a/g'  \
                -e 's/[éèêë]/e/g' \
                -e 's/[îï]/i/g'   \
                -e 's/[ôö]/o/g'   \
                -e 's/[ûü]/u/g'   \
      )

      new_file_path="${file_directory}/${new_file_name}"

      if [[ "$new_file_name" != "$file_name" ]]; then
        renamed_files=$((renamed_files + 1))
        print_line_information " - Renaming $file_path"
        print_line_information "         to $new_file_path"
        mv "$file_path" "$new_file_path"
      fi

    done

    print_line
    print_success_box "Renaming images: Success"
    print_line
    print_success_highlight "Found ${#file_paths[@]} image file(s) to check"
    print_success_highlight "Renamed ${renamed_files} image file(s)"
    print_line
}

function help() {
    echo -e ""
    echo -e "Rename all image files to be compatible with Sphinx."
    echo -e ""
    echo -e "Usage: ./clean-images [OPTIONS]"
    echo -e "  -h, --help    Print this message and exit"
    echo -e ""
    echo -e "Examples:"
    echo -e "  ./clean-images"
    echo -e ""
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    print_line
    print_failure_box "Renaming images: Failed"
    print_line
    print_failure_highlight "An unexpected error has occured!"

    exit 1
}

trap error_handler ERR

main "$@"

exit 0
