#! /bin/bash

# ------------------------------------------------------------------------------
# This file is sourced by the scripts in the same directory.
# ------------------------------------------------------------------------------

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR="$(realpath "$SCRIPT_DIR/..")"

# The version of the Sphinx image
export SPHINX_DOCKER_IMAGE_VERSION="3.2.1-1"

# The name of the container Sphinx is running in
export SPHINX_DOCKER_NAME="$(basename "$PROJECT_DIR")"

# The network interface the host machine Sphinx is bound to
export SPHINX_DOCKER_EXPOSE_INTERFACE=127.0.0.1

# The port on the host machine Sphinx is bound to
export SPHINX_DOCKER_EXPOSE_PORT=8000

# The files Sphinx Live HTML must ignore
export SPHINX_DOCKER_IGNORE_FILES=(
    *.tmp

    *.vdx *.vsx  *.vtx
    *.vsd *.vsdm *.vsdx
    *.vss *.vssm *.vssx
    *.vst *.vstm *.vstx
    *.vsl *.vsw
)
