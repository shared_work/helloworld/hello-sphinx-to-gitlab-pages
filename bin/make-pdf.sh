#!/bin/bash

# ------------------------------------------------------------------------------
# Compile Sphinx documentation to PDF.
#
# Examples:
#   ./make-pdf
# ------------------------------------------------------------------------------

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR="$(realpath "$SCRIPT_DIR/..")"

# shellcheck disable=SC1090
source "$SCRIPT_DIR/variables.sh"
# shellcheck disable=SC1090
source "$SCRIPT_DIR/commons.sh"

function main() {

    if [ "$#" -gt 0 ]; then
        case "$1" in
            -h|--help) help ; exit 0 ;;
            *)         help ; exit 1 ;;
        esac
    fi

    print_line
    print_h1 "Building HTML documentation"
    print_line

    local source_dir="$PROJECT_DIR/source"
    local build_dir="$PROJECT_DIR/build"
    local docker_image="ddidier/sphinx-doc:$SPHINX_DOCKER_IMAGE_VERSION"

    print_h1_highlight "Docker image ....... $docker_image"
    print_h1_highlight "Source directory ... $source_dir"
    print_h1_highlight "Build directory .... $build_dir"
    print_h1_highlight "User ID ............ $UID"
    print_line

    print_step_information "Deleting build directory '$build_dir'"
    rm -rf "$build_dir"
    print_line

    print_step_information "Running Sphinx image '$docker_image'"
    print_line

    local docker_run_interactive=""
    local docker_run_tty=""
    if [ -t 1 ]; then
        docker_run_interactive="-i"
        docker_run_tty="-t"
    fi
    print_line "docker_run_interactive = $docker_run_interactive"
    print_line "docker_run_tty = $docker_run_tty"
    print_line

    start_output_section
    docker run --rm $docker_run_interactive $docker_run_tty \
        -e USER_ID=$UID \
        -v "$PROJECT_DIR":/doc \
        "$docker_image" make latexpdf
    end_output_section
    print_line

    print_success_box "Building PDF documentation: Success"
    print_line
    print_success_highlight "PDF files available at:"
    print_success_highlight "$build_dir/latex/$(find "$build_dir"/latex -name "*.pdf" -printf "%T@ %f\n" | sort | tail -n 1 | cut -d' ' -f2)"
}

function help() {
    echo
    echo -e "Compile Sphinx documentation to PDF."
    echo
    echo -e "Usage: ./make-pdf [OPTIONS]"
    echo -e "  -h, --help    Print this message and exit"
    echo
    echo -e "Examples:"
    echo -e "  ./make-pdf"
    echo
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    print_line
    print_failure_box "Building PDF documentation: Failed"
    print_line
    print_failure_highlight "An unexpected error has occured!"

    exit 1
}

trap error_handler ERR

main "$@"

exit 0
